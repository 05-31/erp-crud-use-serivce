package com.devcamp.api.controller;

import java.util.List;
import com.devcamp.api.model.Product;
import com.devcamp.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProductController {
    
    @Autowired
    ProductService productService;
    
    //GET BY ID 
    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") int id){
        try{
            Product product = productService.getById(id);
            return new ResponseEntity<>(product,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET ALL PAGEABLE
    @GetMapping("/products/pageable/{page}")
    public ResponseEntity<List<Product>> getAllProductsPageable(@PathVariable("page") int page){
        try {
            List<Product> products = productService.getAllPageable(page);
            return new ResponseEntity<>(products,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET ALL PAGEABLE
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(){
        try {
            List<Product> products = productService.getAll();
            return new ResponseEntity<>(products,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY PRODUCT LINE ID
    @GetMapping("/products/product-line-id/{productLineId}")
    public ResponseEntity<List<Product>> getProductsByProductLineId(@PathVariable("productLineId") int productLineId){
        try {
            List<Product> products = productService.getProductByProductLineId(productLineId);
            return new ResponseEntity<>(products,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY PRODUCT SCALE
    @GetMapping("/products/product-scale/{productScale}")
    public ResponseEntity<List<Product>> getProductsByProductScale(@PathVariable("productScale") String productScale){
        try {
            List<Product> products = productService.getProductByProductScale(productScale);
            return new ResponseEntity<>(products,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //POST 
    @PostMapping("/productlines/{productLineId}/products")
    public ResponseEntity<Object> createProduct(@RequestBody Product pProduct,@PathVariable("productLineId") int productLineId){
        try {
            Product product = productService.createProduct(pProduct, productLineId);
            return new ResponseEntity<>(product,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //PUT
    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody Product pProduct,@PathVariable("id") int id){
        try {
            Product product = productService.updateProduct(pProduct, id);
            return new ResponseEntity<>(product,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE ALL
    @DeleteMapping("/products")
    public ResponseEntity<Product> deleteAllCustomers(){
        try {
            productService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteById(@PathVariable("id") int id){
        try {
            productService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
