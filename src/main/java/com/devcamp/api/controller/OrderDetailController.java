package com.devcamp.api.controller;

import java.util.List;
import com.devcamp.api.model.OrderDetail;
import com.devcamp.api.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class OrderDetailController {
    
    @Autowired
    OrderDetailService orderDetailService;

    //GET BY PRODUCT ID
    @GetMapping("/orderdetails/product-id/{productId}")
    public ResponseEntity<List<OrderDetail>> getOrderDetailsByProductId(@PathVariable("productId") int id){
        try {
            List<OrderDetail> orderDetails = orderDetailService.getOrderDetailsByProductId(id);
            return new ResponseEntity<>(orderDetails,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY CUSTOMER ID
    @GetMapping("/orderdetails/order-id/{orderId}")
    public ResponseEntity<List<OrderDetail>> getOrderDetailsByOrderId(@PathVariable("orderId") int id){
        try {
            List<OrderDetail> orderDetails = orderDetailService.getOrderDetailsByOrderId(id);
            return new ResponseEntity<>(orderDetails,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET ALL
    @GetMapping("/orderdetails")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetails(){
        try {
            List<OrderDetail> orderDetails = orderDetailService.getAllOrderDetails();
            return new ResponseEntity<>(orderDetails,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //GET ALL PAGEABLE 
    @GetMapping("/orderdetails/pageable/{page}")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetailsPageable(@PathVariable("page") int page){
        try {
            List<OrderDetail> orderDetails = orderDetailService.getAllPageable(page);
            return new ResponseEntity<>(orderDetails,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //PUT
    @PutMapping("/orderdetails/{id}")
    public ResponseEntity<OrderDetail> updateOrderDetail(@PathVariable("id") int id,@RequestBody OrderDetail pOrderDetail){
        try {
            OrderDetail orderDetail = orderDetailService.updateOrderDetail(pOrderDetail, id);
            return new ResponseEntity<>(orderDetail,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //POST
    @PostMapping("/orderdetails/orders/{orderId}/products/{productId}")
    public ResponseEntity<OrderDetail> createOrderDetail(@PathVariable("orderId") int orderId,@PathVariable("productId") int productId,@RequestBody OrderDetail pOrderDetail){
        try {
            OrderDetail orderDetail = orderDetailService.createOrderDetail(productId, orderId,pOrderDetail);
            return new ResponseEntity<>(orderDetail,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE ALL
    @DeleteMapping("/orderdetails")
    public ResponseEntity<OrderDetail> deleteAllOrderDetails(){
        try {
            orderDetailService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/orderdetails/{id}")
    public ResponseEntity<OrderDetail> deleteOrderDetailById(@PathVariable("id") int id){
        try {
            orderDetailService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
