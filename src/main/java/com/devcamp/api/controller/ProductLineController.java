package com.devcamp.api.controller;

import java.util.List;
import com.devcamp.api.model.ProductLine;
import com.devcamp.api.service.ProductLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineController {
    

    @Autowired
    ProductLineService productLineService;

    //GET BY ID 
    @GetMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id){
        try{
            ProductLine productline = productLineService.getProductLineById(id);
            return new ResponseEntity<>(productline,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET ALL 
    @GetMapping("/productlines")
    public ResponseEntity<List<ProductLine>> getAllProductlines(){
        try {
            List<ProductLine> productlines = productLineService.getAllProductLines();
            return new ResponseEntity<>(productlines,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    

    //POST 
    @PostMapping("/productlines")
    public ResponseEntity<Object> createProductLine(@RequestBody ProductLine pProductLine){
        try {
            ProductLine productline = productLineService.createProductLine(pProductLine);
            return new ResponseEntity<>(productline,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //PUT
    @PutMapping("/productlines/{id}")
    public ResponseEntity<Object> updateProductLine(@RequestBody ProductLine pProductLine,@PathVariable("id") int id){
        try {
            ProductLine productline = productLineService.updateProductLine(pProductLine,id);
            return new ResponseEntity<>(productline,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE ALL
    @DeleteMapping("/productlines")
    public ResponseEntity<ProductLine> deleteAllProductLine(){
        try {
            productLineService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") int id){
        try {
            productLineService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
