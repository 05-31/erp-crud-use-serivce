package com.devcamp.api.controller;

import java.util.List;
import com.devcamp.api.model.Order;
import com.devcamp.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    
    @Autowired
    OrderService orderService;

    //GET ALL
    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders(){
        try {
            List<Order> allOrders = orderService.getAllOrders();
            return new ResponseEntity<>(allOrders,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY STATUS
    @GetMapping("/orders/status/{status}")
    public ResponseEntity<List<Order>> getOrdersByStatus(@PathVariable("status") String status){
        try {
            List<Order> orders = orderService.getOrdersByStatus(status);
            return new ResponseEntity<>(orders,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY CUSTOMER ID
    @GetMapping("/orders/customer-id/{customerId}")
    public ResponseEntity<List<Order>> getOrdersByCustomerId(@PathVariable("customerId") int customerId){
        try {
            List<Order> orders = orderService.getOrdersByCustomerId(customerId);
            return new ResponseEntity<>(orders,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //POST 
    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@RequestBody Order pOrder){
        try {
            Order newOrder = orderService.createOrder(pOrder);
            return new ResponseEntity<>(newOrder,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //PUT
    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@RequestBody Order pOrder,@PathVariable("id") int id){
        try {
            Order newOrder = orderService.updateOrder(pOrder,id);
            return new ResponseEntity<>(newOrder,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE ALL 
    @DeleteMapping("/orders")
    public ResponseEntity<Order> deleteAll(){
        try {
            orderService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Order> deleteById(@PathVariable("id") int id){
        try {
            orderService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
