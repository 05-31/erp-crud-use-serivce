package com.devcamp.api.service;

import java.util.List;
import java.util.Optional;
import com.devcamp.api.model.Customer;
import com.devcamp.api.repository.CustomerRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    
    @Autowired
    CustomerRepository customerRepository;

    //GET ALL
    public List<Customer> getAllCustomers(){
        List<Customer> allCustomers = new ArrayList<>();
        customerRepository.findAll().forEach(allCustomers::add);
        return allCustomers;
    }

    //GET BY PHONE NUMBER
    public List<Customer> getCustomerByPhoneNumber(String phoneNumber){
        List<Customer> customers = new ArrayList<>();
        customerRepository.findByPhoneNumber(phoneNumber).forEach(customers::add);
        return customers;
    }

    //GET BY COUNTRY 
    public List<Customer> getByCountry(String country){
        List<Customer> customers = new ArrayList<>();
        customerRepository.findByCountry(country).forEach(customers::add);
        return customers;
    }

    //GET BY CITY
    public List<Customer> getByCity(String city){
        List<Customer> customers = new ArrayList<>();
        customerRepository.findByCity(city).forEach(customers::add);
        return customers;
    }
    
    //GET BY ID
    public Customer getById(int id){
        Optional<Customer> findCustomer = customerRepository.findById(id);
        Customer customerData = findCustomer.get();
        return customerData;
    }

    //POST 
    public Customer createCustomer(Customer pCustomer){
        Customer newCustomer = new Customer();
        newCustomer.setLastName(pCustomer.getLastName());
        newCustomer.setFirstName(pCustomer.getFirstName());
        newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
        newCustomer.setAddress(pCustomer.getAddress());
        newCustomer.setCity(pCustomer.getCity());
        newCustomer.setState(pCustomer.getState());
        newCustomer.setPostalCode(pCustomer.getPostalCode());
        newCustomer.setCountry(pCustomer.getCountry());
        newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
        newCustomer.setCreditLimit(pCustomer.getCreditLimit());
        Customer saveCustomer = customerRepository.save(newCustomer);
        return saveCustomer;
    }

    //DELETE ALL
    public void deleteAll(){
        customerRepository.deleteAll();
    }
    
    //DELETE BY ID
    public void deleteById(int id){
        customerRepository.deleteById(id);
    }

    //UPDATE 
    public Customer updateCustomer(Customer pCustomer,int id){
        Optional<Customer> findCustomer = customerRepository.findById(id);
        Customer customerData = findCustomer.get();
        customerData.setLastName(pCustomer.getLastName());
        customerData.setFirstName(pCustomer.getFirstName());
        customerData.setPhoneNumber(pCustomer.getPhoneNumber());
        customerData.setAddress(pCustomer.getAddress());
        customerData.setCity(pCustomer.getCity());
        customerData.setState(pCustomer.getState());
        customerData.setPostalCode(pCustomer.getPostalCode());
        customerData.setCountry(pCustomer.getCountry());
        customerData.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
        customerData.setCreditLimit(pCustomer.getCreditLimit());
        Customer saveCustomer = customerRepository.save(customerData);
        return saveCustomer;
    }

}
