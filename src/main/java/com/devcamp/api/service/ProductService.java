package com.devcamp.api.service;

import java.util.List;
import java.util.Optional;
import com.devcamp.api.model.Product;
import com.devcamp.api.model.ProductLine;
import com.devcamp.api.repository.ProductLineRepository;
import com.devcamp.api.repository.ProductRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class ProductService {
    
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductLineRepository productLineRepository;

    //GET BY PRODUCT LINE ID
    public List<Product> getProductByProductLineId(int productLineId){
        List<Product> products = new ArrayList<>();
        productRepository.findByProductLineId(productLineId).forEach(products::add);
        return products;
    }

    //GET BY PRODUCT SCALE
    public List<Product> getProductByProductScale(String productScale){
        List<Product> products = new ArrayList<>();
        productRepository.findByProductScale(productScale).forEach(products::add);
        return products;
    }

    //GET ALL PAGEABLE
    public List<Product> getAllPageable(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page){
        List<Product> products = new ArrayList<>();
        productRepository.findAll(PageRequest.of(page,10)).forEach(products::add);
        return products;
    }

    //GET ALL
    public List<Product> getAll(){
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    //GET BY ID
    public Product getById(int id){
        Optional<Product> findProduct = productRepository.findById(id);
        Product productData = findProduct.get();
        return productData;
    }
    
    //POST
    public Product createProduct(Product pProduct,int productLineId){
        Optional<ProductLine> findProductLine = productLineRepository.findById(productLineId);
        ProductLine productLineData = findProductLine.get();
        Product newProduct = new Product();
        newProduct.setProductCode(pProduct.getProductCode());
        newProduct.setProductName(pProduct.getProductName());
        newProduct.setProductDescripttion(pProduct.getProductDescripttion());
        newProduct.setProductScale(pProduct.getProductScale());
        newProduct.setProductVendor(pProduct.getProductVendor());
        newProduct.setQuantityInStock(pProduct.getQuantityInStock());
        newProduct.setBuyPrice(pProduct.getBuyPrice());
        newProduct.setProductLine(productLineData);

        Product saveProduct = productRepository.save(newProduct);
        return saveProduct;
    }

    //PUT 
    public Product updateProduct(Product pProduct,int id){
        Optional<Product> findProduct = productRepository.findById(id);
        Product productData = findProduct.get();
        productData.setProductCode(pProduct.getProductCode());
        productData.setProductName(pProduct.getProductName());
        productData.setProductDescripttion(pProduct.getProductDescripttion());
        productData.setProductScale(pProduct.getProductScale());
        productData.setProductVendor(pProduct.getProductVendor());
        productData.setQuantityInStock(pProduct.getQuantityInStock());
        productData.setBuyPrice(pProduct.getBuyPrice());

        Product saveProduct = productRepository.save(productData);
        return saveProduct;
    }

    //DELETE ALL
    public void deleteAll(){
        productRepository.deleteAll();
    }
    
    //DELETE BY ID
    public void deleteById(int id){
        productRepository.deleteById(id);
    }
    
}
