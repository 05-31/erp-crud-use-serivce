package com.devcamp.api.service;

import java.util.List;
import java.util.Optional;
import com.devcamp.api.model.ProductLine;
import com.devcamp.api.repository.ProductLineRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductLineService {
    
    @Autowired
    ProductLineRepository productLineRepository;

    //GET ALL
    public List<ProductLine> getAllProductLines(){
        List<ProductLine> allProductLines = new ArrayList<>();
        productLineRepository.findAll().forEach(allProductLines::add);
        return allProductLines;
    }

    //GET BY ID
    public ProductLine getProductLineById(int id){
        Optional<ProductLine> findProductLine = productLineRepository.findById(id);
        ProductLine productLineData = findProductLine.get();
        return productLineData;
    }
    
    //POST
    public ProductLine createProductLine(ProductLine pProductLine){
        ProductLine newProductLine = new ProductLine();
        newProductLine.setDescription(pProductLine.getDescription());
        newProductLine.setProductLine(pProductLine.getProductLine());
        newProductLine.setProducts(pProductLine.getProducts());

        ProductLine saveProductLine  = productLineRepository.save(newProductLine);
        return saveProductLine;
    }

    //PUT 
    public ProductLine updateProductLine(ProductLine pProductLine,int id){
        Optional<ProductLine> findProductLine = productLineRepository.findById(id);
        ProductLine productDataLine = findProductLine.get();
        productDataLine.setDescription(pProductLine.getDescription());
        productDataLine.setProductLine(pProductLine.getProductLine());
        productDataLine.setProducts(pProductLine.getProducts());

        ProductLine saveProductLine = productLineRepository.save(productDataLine);
        return saveProductLine;
    }

    //DELETE ALL
    public void deleteAll(){
        productLineRepository.deleteAll();
    }
    
    //DELETE BY ID
    public void deleteById(int id){
        productLineRepository.deleteById(id);
    }
    
}
