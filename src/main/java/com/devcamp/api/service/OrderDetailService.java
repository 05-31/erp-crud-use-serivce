package com.devcamp.api.service;

import com.devcamp.api.model.Order;
import com.devcamp.api.model.OrderDetail;
import com.devcamp.api.model.Product;
import com.devcamp.api.repository.OrderDetailRepository;
import com.devcamp.api.repository.OrderRepository;
import com.devcamp.api.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderDetailService {
    
    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    //GET ALL
    public List<OrderDetail> getAllOrderDetails(){
        List<OrderDetail> allOrderDetails = new ArrayList<>();
        orderDetailRepository.findAll().forEach(allOrderDetails::add);
        return allOrderDetails;
    }

    //GET BY ORDER ID
    public List<OrderDetail> getOrderDetailsByOrderId(int id){
        List<OrderDetail> allOrderDetails = new ArrayList<>();
        orderDetailRepository.findByOrderId(id).forEach(allOrderDetails::add);
        return allOrderDetails;
    }

    //GET BY PRODUCT ID
    public List<OrderDetail> getOrderDetailsByProductId(int id){
        List<OrderDetail> allOrderDetails = new ArrayList<>();
        orderDetailRepository.findByProductId(id).forEach(allOrderDetails::add);
        return allOrderDetails;
    }

    //GET ALL PAGEABLE
    public List<OrderDetail> getAllPageable(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page){
        List<OrderDetail> allOrderDetails = new ArrayList<>();
        orderDetailRepository.findAll(PageRequest.of(page,10)).forEach(allOrderDetails::add);
        return allOrderDetails;
    }

    //PUT 
    public OrderDetail updateOrderDetail(OrderDetail pOrderDetail,int id){
        Optional<OrderDetail> findOrderDetail = orderDetailRepository.findById(id);
        OrderDetail orderDetailData = findOrderDetail.get();
        orderDetailData.setQuantityOrder(pOrderDetail.getQuantityOrder());
        orderDetailData.setPriceEach(pOrderDetail.getPriceEach());
        OrderDetail saveOrderDetail = orderDetailRepository.save(orderDetailData);
        return saveOrderDetail;
    }

    //POST
    public OrderDetail createOrderDetail(int productId,int orderId,OrderDetail pOrderDetail){
        Optional<Order> findOrder = orderRepository.findById(orderId);
        Order orderData = findOrder.get();
        Optional<Product> findProduct = productRepository.findById(productId);
        Product productData = findProduct.get();
        OrderDetail newOrderDetail = new OrderDetail();
        newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
        newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
        newOrderDetail.setProduct(productData);
        newOrderDetail.setOrder(orderData);

        OrderDetail saveOrderDetail = orderDetailRepository.save(newOrderDetail);
        return saveOrderDetail;
    }

    //DELETE ALL
    public void deleteAll(){
        orderDetailRepository.deleteAll();
    }
    
    //DELETE BY ID
    public void deleteById(int id){
        orderDetailRepository.deleteById(id);
    }

}
