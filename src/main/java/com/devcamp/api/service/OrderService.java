package com.devcamp.api.service;

import com.devcamp.api.repository.OrderRepository;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import com.devcamp.api.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    
    @Autowired
    OrderRepository orderRepository;

    //GET ALL
    public List<Order> getAllOrders(){
        List<Order> allOrders = new ArrayList<>();
        orderRepository.findAll().forEach(allOrders::add);
        return allOrders;
    }

    //GET BY STATUS
    public List<Order> getOrdersByStatus(String status){
        List<Order> allOrders = new ArrayList<>();
        orderRepository.findByStatus(status).forEach(allOrders::add);
        return allOrders;
    }

    //GET BY CUSTOMER ID
    public List<Order> getOrdersByCustomerId(int customerId){
        List<Order> allOrders = new ArrayList<>();
        orderRepository.findByCustomerId(customerId).forEach(allOrders::add);
        return allOrders;
    }

    //POST 
    public Order createOrder(Order pOrder){
        Order newOrder = new Order();
        newOrder.setOrderDate(pOrder.getOrderDate());
        newOrder.setRequiredDate(pOrder.getOrderDate());
        newOrder.setShippedDate(pOrder.getShippedDate());
        newOrder.setStatus(pOrder.getStatus());
        newOrder.setComments(pOrder.getComments());
        newOrder.setCustomer(pOrder.getCustomer());

        Order saveOrder = orderRepository.save(newOrder);
        return saveOrder;
    }

    //PUT
    public Order updateOrder(Order pOrder, int id){
        Optional<Order> findOrder = orderRepository.findById(id);
        Order orderData = findOrder.get();
        orderData.setOrderDate(pOrder.getOrderDate());
        orderData.setRequiredDate(pOrder.getOrderDate());
        orderData.setShippedDate(pOrder.getShippedDate());
        orderData.setStatus(pOrder.getStatus());
        orderData.setComments(pOrder.getComments());
        orderData.setOrderDetails(pOrder.getOrderDetails());
        Order saveOrder = orderRepository.save(orderData);
        return saveOrder;
    }

    //DELTE ALL
    public void deleteAll(){
        orderRepository.deleteAll();
    }

    //DELETE BY ID
    public void deleteById(int id){
        orderRepository.deleteById(id);
    }
}
