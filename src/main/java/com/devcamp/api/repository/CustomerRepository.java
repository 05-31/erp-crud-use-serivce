package com.devcamp.api.repository;

import java.util.List;
import com.devcamp.api.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer,Integer>{
    
    //GET BY PHONE NUMBER
    @Query(value = "SELECT * FROM customers WHERE phone_number LIKE :phoneNumber%",nativeQuery = true)
    List<Customer> findByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    //GET BY COUNTRY
    @Query(value = "SELECT * FROM customers WHERE country LIKE :country%",nativeQuery = true)
    List<Customer> findByCountry(@Param("country") String country);

    //GET BY CITY
    @Query(value = "SELECT * FROM customers WHERE city LIKE :city%",nativeQuery = true)
    List<Customer> findByCity(@Param("city") String city);

    //COUNT CUSTOMER IN COUNTRY
    @Query(value = "SELECT DISTINCT(`country`) AS countryName,COUNT(id) AS amountCountry FROM `customers` GROUP BY `country`",nativeQuery = true)
    List countCustomerInCountry();

}
