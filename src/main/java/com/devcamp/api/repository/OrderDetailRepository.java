package com.devcamp.api.repository;

import java.util.List;
import com.devcamp.api.model.OrderDetail;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderDetailRepository extends JpaRepository<OrderDetail,Integer>{
    
    @Query(value = "SELECT * FROM order_details WHERE order_id LIKE :orderId%",nativeQuery = true)
    List<OrderDetail> findByOrderId(@Param("orderId") int orderId);

    @Query(value = "SELECT * FROM order_details WHERE product_id LIKE :productId%",nativeQuery = true)
    List<OrderDetail> findByProductId(@Param("productId") int productId);

    @Query(value = "SELECT * FROM order_details",nativeQuery = true)
    List<OrderDetail> findAll(Pageable pageable);

}

