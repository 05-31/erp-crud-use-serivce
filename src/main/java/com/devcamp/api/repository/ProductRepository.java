package com.devcamp.api.repository;

import java.util.List;

import com.devcamp.api.model.Product;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product,Integer>{
    
    @Query(value = "SELECT * FROM products WHERE product_line_id LIKE :productLineId%",nativeQuery = true)
    List<Product> findByProductLineId(@Param("productLineId") int productLineId);
    
    @Query(value = "SELECT * FROM products",nativeQuery = true)
    List<Product> findAll(Pageable pageable);
    
    @Query(value = "SELECT * FROM products WHERE product_scale LIKE :productScale%",nativeQuery = true)
    List<Product> findByProductScale(@Param("productScale") String productLineId);
    
}
