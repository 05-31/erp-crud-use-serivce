package com.devcamp.api.repository;

import com.devcamp.api.model.ProductLine;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductLineRepository extends JpaRepository<ProductLine,Integer>{
    
}
